<?php

//Custom Post Type
function custom_post_example() {
    $labels = array(
        'name'               => _x( 'Examples', 'post type general name' ),
        'singular_name'      => _x( 'Example', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'employee' ),
        'add_new_item'       => __( 'Add New Example' ),
        'edit_item'          => __( 'Edit Example' ),
        'new_item'           => __( 'New Example' ),
        'all_items'          => __( 'All Examples' ),
        'view_item'          => __( 'View Example' ),
        'search_items'       => __( 'Search Examples' ),
        'not_found'          => __( 'No Examples found' ),
        'not_found_in_trash' => __( 'No Examples found in the Trash' ),
        'hierarchical'       => TRUE,
        'parent_item_colon'  => '',
        'menu_name'          => 'Example Posts',
        'show_admin_column' => true,
    );
    $args = array(
        'labels'        => $labels,
        'description'   => 'Example Post Type',
        'public'        => true,
        'taxonomies' => array('post_tag'),
        'menu_position' => 6,
        'hierarchical' => true,
        'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'author' ),
        'has_archive'   => false,
    );
    register_post_type( 'example', $args );
    flush_rewrite_rules( false );
}
add_action( 'init', 'custom_post_example' );

//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_example_cats', 0 );

//create a custom taxonomy name it topics for your posts

function create_example_cats() {

// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI

    $labels = array(
        'name' => _x( 'Categories', 'taxonomy general name' ),
        'singular_name' => _x( 'Category', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Categories' ),
        'all_items' => __( 'All Categories' ),
        'parent_item' => __( 'Parent Category' ),
        'parent_item_colon' => __( 'Parent Category:' ),
        'edit_item' => __( 'Edit Category' ),
        'update_item' => __( 'Update Category' ),
        'add_new_item' => __( 'Add Category' ),
        'new_item_name' => __( 'New Category' ),
        'menu_name' => __( 'Categories' ),
    );

// Now register the taxonomy

    register_taxonomy('categories',array('example'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'cat' ),
    ));

}

add_filter( 'cmb_meta_boxes', 'cmb_example_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function cmb_example_metaboxes( array $meta_boxes ) {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_cmb_';

    $meta_boxes[] = array(
        'id'         => 'ex_metabox',
        'title'      => 'Example Details',
        'pages'      => array( 'example', ), // Post type
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
        'fields'     => array(
            array(
                'name' => 'Text',
                'id'   => $prefix . 'example_text',
                'type' => 'text',
            ),
            array(
                'name' => "Example Editor",
                'id'   => $prefix . 'employee_bag',
                'type' => 'wysiwyg',
            ),
        )
    );

    return $meta_boxes;
}