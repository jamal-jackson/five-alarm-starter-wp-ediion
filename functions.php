<?php

require_once "libs/include.php";

//Add support for WordPress 3.0's custom menus
add_action( 'init', 'register_my_menu' );

//Register area for custom menu
function register_my_menu() {
    register_nav_menus( array(
        'Main' => 'Header Navigation Menu',
        'footer_menu' => 'Footer Menu'
    ) );
}

// Add support for post thumbnails
add_theme_support( 'post-thumbnails' );

// Create Sidebars
if ( function_exists('register_sidebar') ){
    register_sidebar(array('name'=>'Blog',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));
}

// Return List of Post Tags
function return_tags(){
    $posttags = get_the_tags();
    $tags_count = count($posttags) - 1;
    $i = 0;
    if ($posttags) {
        foreach($posttags as $tag) {
            if( $i == $tags_count ){ $v = ' ';}else{ $v = ', '; }
            echo "<a href='" . get_tag_link($tag->term_id) . "'>" . $tag->name . "</a>" . $v;
            $i++;
        }
    }
}

//Disqus Account embed function
function disqus_embed($disqus_shortname) {
    global $post;
    wp_enqueue_script('disqus_embed','https://'.$disqus_shortname.'.disqus.com/embed.js');
    echo '<div id="disqus_thread"></div>
    <script type="text/javascript">
        var disqus_shortname = "'.$disqus_shortname.'";
        var disqus_title = "'.$post->post_title.'";
        var disqus_url = "'.get_permalink($post->ID).'";
        var disqus_identifier = "'.$disqus_shortname.'-'.$post->ID.'";
    </script>';
}

// Edits output of tag cloud widget
add_filter('widget_tag_cloud_args', 'tag_widget_limit');
function tag_widget_limit($args){
    if(isset($args['taxonomy']) && $args['taxonomy'] == 'post_tag'){
        $args['number'] = 5; //Limit number of tags
        $args['unit'] = 'em';
        $args['smallest'] = '0.9';
        $args['largest'] = '0.9';
    }
    return $args;
}

//Sets Content Limit to Feed
function content($limit) {
    $content = explode(' ', get_the_content(), $limit);
    if (count($content)>=$limit) {
        array_pop($content);
        $content = implode(" ",$content).'...';
    } else {
        $content = implode(" ",$content);
    }
    $content = preg_replace('/\[.+\]/','', $content);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    return $content;
}