        <footer>
            <?php wp_nav_menu( array('menu' => 'footer_menu', 'container' => false, )); ?>
            &copy; Five Alarm Interactive | MIEN. All Rights Reserved.
        </footer>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/app.js"></script>
        <?php wp_footer(); ?>
    </body>
</html>