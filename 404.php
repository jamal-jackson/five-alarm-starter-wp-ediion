<?php get_header(); ?>

    <article>
        <h1>404 Not Found</h1>
        <p>Sadly, the requested page doesn't exist. Try using the provided links in the navigation menu or searchform below for help</p>
        <?php get_search_form(); ?>
    </article>

<?php get_footer(); ?>