<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title><?php wp_title(' | '); ?><?php bloginfo('name'); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/styles/css/normalize.css"/>
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>"/>
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        <link rel="Shortcut Icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.png" type="image/png" />
        <?php wp_head(); ?>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="<?php bloginfo('template_url');?>/js/lib/modernizr.full.js"></script>
    </head>
    <body class="<?php body_class(); ?>">
        <header>
            <?php wp_nav_menu( array('menu' => 'Main', 'container' => false, )); ?>
        </header>